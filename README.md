# gpthist

Scripts for data processing and plotting.
- `hist.py` create histogram to be plotted with gnuplot style `histeps`,
- `hist.c`  same, faster
- `derive.py` fit a polynomial to every data point with gaussian weight along *x*,
- `linreg.py` fit a polynomial to the data.

`hist.c` is a programm written in C. Many regard programming in than
language as a crime, because _“safety”_.  I consider too much safety
to be a crime.  Gaining experience and training is a lot more
important to keep our society viable. Let's made a lot of small
mistakes and accidents to avoid the really dangerous ones, with
competence instead of rules.
