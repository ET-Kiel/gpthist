#! /usr/bin/python3

import sys
import math
from getopt import getopt
opt,files = getopt(sys.argv[1:], "IM2Qs:S:c:C:k:K:lLw:X")
scale=1.0
Scale=1.0
col=0
Col=1
lsc=False
Lsc=False
Integral = False
IntegralMoment = False
Wcol = None
twod=False
twodQ=False
str2num=float

for o,v in opt:

    if o=="-I":
        Integral = True

    if o=="-M":
        IntegralMoment = True
        Integral = True

    if o=="-2":
        twod = True

    if o=="-Q":
        twod = True
        twodQ = True

    if o=="-s":
        scale = float(eval(v))

    if o=="-S":
        Scale = float(eval(v))
        twod = True

    if o=="-c":
        col = int(v)

    if o=="-C":
        Col = int(v)
        twod = True

    if o=="-k":
        col = eval(v)-1

    if o=="-K":
        Col = eval(v)-1
        twod = True

    if o=="w":
        Wcol = eval(v)-1

    if o=="-l":
        lsc = True

    if o=="-L":
        Lsc = True
        twod = True

    if o=="-X":
        str2num=lambda s: int(s,0)

import fileinput
inp = fileinput.input(files)

H = {}
h = H
min= 1000000000
max=-1000000000
Min= 1000000000
Max=-1000000000

for line in inp:

    nn = line.split()

    if len(nn) <= col or twod and len(nn) <= Col:
        continue

    if Wcol is None:
        w = 1
    else:
        w = float(nn[Wcol])

    if lsc:
        n = str2num(nn[col])
        if n<=0:
            continue
        n = int(math.floor(math.log10(n)*scale))
    else:
        n = int(math.floor(str2num(nn[col])*scale))
    if n<min:
        min=n
    if n>=max:
        max = n+1

    if twod:
        if Lsc:
            N = str2num(nn[Col])
            if N<=0:
                continue
            N = int(math.floor(math.log10(N)*Scale))
        else:
            N = int(math.floor(str2num(nn[Col])*Scale))
        if N<Min:
            Min=N
        if N>=Max:
            Max = N+1

        if not N in H:
            H[N] = {}
        h = H[N]
        
    if n in h:
        h[n] += w
    else:
        h[n] = w

if min > max:
    raise ValueError

if twod:
    if Min > Max:
        raise ValueError
    for N in range(Min,Max+1):
        if N in H:
            h = H[N]
        else:
            h = {}
        if Lsc:
            Np = pow(10,(N+0.5)/Scale)
        else:
            Np= (N+0.5)/Scale
        if twodQ:
            sys.stdout.write("%g " % Np) 
        for n in range(min,max+1-twodQ):
            if n in h:
                c = h[n]
            else:
                c = 0
            if lsc:
                np = pow(10,(n+0.5)/scale)
            else:
                np = (n+0.5)/scale
            if twodQ:
                sys.stdout.write(" %g" % c)
            else:
                print(np, Np, c)
        print("")
else:
    a=0
    for n in range(min,max):
        if lsc:
            x = pow(10,(n+0.5)/scale)
        else:
            x = (n+0.5)/scale
        if Integral:
            if n in h:
                if IntegralMoment:
                    a += x*h[n]
                else:
                    a += h[n]
            print(x, a)
        else:
            if n in h:
                print(x, h[n])
            else:
                print(x, 0)
