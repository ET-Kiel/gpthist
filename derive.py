#! /usr/bin/python3

"""%s «options» «files»

Read data points (x,y1,y2,…) from «files» or «stdin» and perform 
polynomial fits of degree «p» y=P(x) around each x with a gaussian weight
of width «s» centered at x.  Output the first two coefficients, i.e., the
smoothed value y and the slope dy/dx.

Options:
  -P        column indices from 0 (Python)
  -K        column indices from 1 (Default)
  -x «ix»   column number of the x value
  -y «iy»   column number (range) of y values, repeat or comma separated
  -s «s»    width in x of the gaussian weight, default -s 1
  -p «p»    degree of the polynominal, default -p 1
  -l        use the natural logarithm of y
  -f «sep»  column seperator, default whitespace.
  -d        debug output, if any
  -v        verbose output to «stderr»
  -F «fmt»  numbers format (printf float), default %%g
  -N «nc»   number of coefficients to emit, default -N 2
  -S «s»    fit range in units of sigma
  -W «step» minimum distance of x-values
  -w «step» increment of output x-values (implies -N2)
  -h        print this help

If -y is given, but no -x, the row number is used for x.  When no -y is 
given, but -x is given, use «iy»=«ix»+1 for a single y column, else use 
the first column for x and the remaining columns as y,  unless there is 
only one column, then use that for y. 

If neither -y nor -x are given, the first line of input must have the 
correct number of columns.  Any line where any data item cannot be 
parsed into a number will be ignored. 

-y columns may be given in ranges c₁-c₂[-s] from column c₁ up to c₂
(exclusive), in steps of s (optional).

The output format is    x y₁ y₂ … P₁(x) dP₁(x) P₂(x) dP₂(x) …

""" 

import sys, math, numpy, getopt, fileinput

def usage():
    sys.stderr.write(__doc__ % (sys.argv[0],))

try:
    opt,files = getopt.getopt(sys.argv[1:], "PKx:y:s:lLvd:p:hf:N:F:S:W:w:")
except:
    usage()
    raise
    
sigma=1.0
ix=-1
iy=[]
logscale=False
verbose=False
debug=False
P=1
sep=None
fmt="%.10g"
N=2
fit_range=5.
minstep=0.
xstep=None

i_base = 1

numpy.set_printoptions(linewidth=100, precision=4)

for o,v in opt:
    try:

        if o=="-K":
            i_base = 1
        if o=="-P":
            i_base = 0
        
        if o=="-x":
            ix = int(v) - i_base

        if o=="-y":
            for vv in v.split(','):
                vvv = vv.split('-')
                if len(vvv)==1:
                    iy.append(int(vvv[0]) - i_base)
                else:
                    iy.extend(numpy.arange(*map(int,vvv)) - i_base)

        if o=="-s":
            sigma = float(eval(v))

        if o=="-S":
            fit_range = float(eval(v))

        if o=="-W":
            minstep = float(eval(v))

        if o=="-w":
            xstep = float(eval(v))

        if o=="-l":
            logscale = True
        
        if o=="-v":
            verbose = True
        
        if o=="-d":
            debug = True
        
        if o=="-p":
            P = int(v)

        if o=="-f":
            sep = v
        
        if o=="-F":
            fmt = v

        if o=="-N":
            N = int(v)
            
        if o=="-h":
            usage()
            sys.exit()

    except Exception:
        usage()
        sys.stderr.write("Error parsing option: %s %s\n" % (o,v))
        raise
    
if N>P:
    N=P+1
cfmt=" ".join([fmt]*N)

XY=[]
i=0

if verbose:
    sys.stderr.write("ix=%s iy=%s\n" % (ix, iy))

for line in fileinput.input(files):
    nn = line.split(sep)
    if not iy:
        if ix<0:
            if len(nn)<2:
                iy=[0,]
            else:
                ix=0
                iy=list(range(1,len(nn)))
        else:
            iy=[ix+1]
    try:
        if ix<0:
            x = [i]
        else:
            x = [float(eval(nn[ix]))]
        x += [float(eval(nn[i])) for i in iy]
        XY.append(x)
        i += 1
    except Exception as e:
        if verbose:
            sys.stderr.write("before item %d: %s\n" % (i,line.strip()))
            sys.stderr.write(repr(e)+"\n")

if not XY or verbose:
    sys.stderr.write("read %d items\n" % i)

XY.sort()
            
XY=numpy.array(XY)
x = XY[:,0]
y = XY[:,1:].transpose()
if logscale:
    y = numpy.log(y, out=y)

fit_range *= sigma
sigma = 1/(math.sqrt(2)*sigma)

p = numpy.arange(2*P+1).reshape((-1,1,1))
Pr = numpy.arange(P+1)
Ai = Pr+Pr.reshape((-1,1))

i1=0
i2=0

nn = XY.shape[0]
last_x = x[0]
if xstep:
    next_x = xstep * math.floor(last_x/xstep-1)

for i in range(nn):
    if x[i] < last_x+minstep:
        continue
    last_x = x[i]
    if i2<nn and x[i2] < x[i]+fit_range:
        x1  = None
        y1  = None
        xx  = None
        xy  = None
        w   = None
        w   = None
        w   = None
        w   = None
        wxx = None
        wxy = None
        sxx = None
        sxy = None
        ii1=i1
        ii2=i2
        i1 = (ii1+ii2)//2
        while i1 > ii1:
            if x[i1] + fit_range > x[i]:
                ii2 = i1
            else:
                ii1 = i1
            i1 = (ii1+ii2)//2
        ii1 = i2
        ii2 = nn
        i2 = (ii1+ii2)//2
        while i2 > ii1:
            if x[i2] > x[i]+2*fit_range:
                ii2 = i2
            else:
                ii1 = i2
            i2 = (ii1+ii2)//2

    if i2-i1 <= P:
        sys.stdout.write("\n")
        continue

    x1  = numpy.add(x[i1:i2], -x[i], out=x1)
    y1  = numpy.add(y[:,i1:i2], -y[:,i].reshape((-1,1)), out=y1)
    xx  = numpy.power(x1, p, out=xx)
    xy  = numpy.multiply(xx[:P+1], y1, out=xy)
    w   = numpy.multiply(x1, sigma, out=w)
    w   = numpy.multiply(w, w, out=w)
    w   = numpy.negative(w, out=w)
    w   = numpy.exp(w, out=w)
    wxx = numpy.multiply(w, xx, out=wxx)
    wxy = numpy.multiply(w, xy, out=wxy)
    sxx = numpy.add.reduce(wxx, axis=-1, out=sxx)
    sxy = numpy.add.reduce(wxy, axis=-1, out=sxy)
    d   = numpy.linalg.solve(sxx.reshape((-1,))[Ai], sxy)
    d[0] += y[:,i]

    if xstep:
        if next_x < x[i] - xstep:
            next_x =  xstep * math.floor(x[i]/xstep - 1)
        while next_x < x[i]:
            next_x += xstep
            xint = next_x - x[i]
            yint = sum(d * xint**Pr.reshape((-1,1)))
            dint = sum(d[1:] * (xint**Pr[:-1] * Pr[1:]).reshape((-1,1)))
            sys.stdout.write((fmt+" %s %s\n") %
                             ( next_x,
                               " ".join([fmt % yy for yy in yint]),
                               " ".join([fmt % yy for yy in dint]),
                              ))
        continue

    if logscale:
        d[0] = numpy.exp(d[0])
        # d[1] *= d[0]
    sys.stdout.write((fmt+" %s %s\n") %
                     ( x[i],
                       " ".join([fmt % yy for yy in y[:,i]]),
                       " ".join([cfmt % tuple(dd[:N])
                                 for dd in d.transpose()])
                     ))
