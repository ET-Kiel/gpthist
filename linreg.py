#! /usr/bin/python3

"""%s «options» «files»

Read data points (x,y1,y2,…) from «files» or «stdin» and perform 
polynomial fits of degree «p» y=P(x) around each x with a gaussian weight
of width «s» centered at x.  Output the first two coefficients, i.e., the
smoothed value y and the slope dy/dx.

Options:
  -P        column indices from 0 (Python)
  -K        column indices from 1 (Default)
  -x «ix»   column number of the x value
  -y «iy»   column number (range) of y values, repeat or comma separated
  -w «iw»   column number for weight(s)
  -s «iw»   column number for weight(s)
  -σ «iw»   column number for weight(s)
  -p «p»    degree of the polynominal, default -p 1
  -l        use the natural logarithm of y
  -f «sep»  column seperator, default whitespace.
  -d        debug output, if any
  -v        verbose output to «stderr»
  -F «fmt»  numbers format (printf float), default %%g
  -h        print this help

If -y is given, but no -x, the row number is used for x.  When no -y is 
given, but -x is given, use «iy»=«ix»+1 for a single y column, else use 
the first column for x and the remaining columns as y,  unless there is 
only one column, then use that for y. 

If neither -y nor -x are given, the first line of input must have the 
correct number of columns.  Any line where any data item cannot be 
parsed into a number will be ignored. 

If -w weight colums are given it must either be one or the same number
of columns as -y.  If weight are given with -s or -σ the columns are
errors, w=1/σ².

-y and -w columns may be given in ranges c₁-c₂[-s] from column c₁ up
to c₂ (exclusive), in steps of s (optional).

The output format is    x y₁ y₂ … P₁(x) dP₁(x) P₂(x) dP₂(x) …

""" 

import sys, math, numpy, getopt, fileinput

def usage():
    sys.stderr.write(__doc__ % (sys.argv[0],))

try:
    opt,files = getopt.getopt(sys.argv[1:], "PKx:y:s:σ:w:lLvd:p:hf:F:S:W:")
except:
    usage()
    raise
    
ix=-1
iy=[]
iw=[]
isσ = False
logscale=False
verbose=False
debug=False
P=1
sep=None
fmt="%.10g"

i_base = 1

numpy.set_printoptions(linewidth=100, precision=4)

for o,v in opt:
    try:

        if o=="-K":
            i_base = 1
        if o=="-P":
            i_base = 0
        
        if o=="-x":
            ix = int(v) - i_base

        if o=="-y":
            for vv in v.split(','):
                vvv = vv.split('-')
                if len(vvv)==1:
                    iy.append(int(vvv[0]) - i_base)
                else:
                    iy.extend(numpy.arange(*map(int,vvv)) - i_base)

        if o in "-s -σ":
            isσ = True
            
        if o in "-s -σ -w":
            for vv in v.split(','):
                vvv = vv.split('-')
                if len(vvv)==1:
                    iw.append(int(vvv[0]) - i_base)
                else:
                    iw.extend(numpy.arange(*map(int,vvv)) - i_base)

        if o=="-l":
            logscale = True
        
        if o=="-v":
            verbose = True
        
        if o=="-d":
            debug = True
        
        if o=="-p":
            P = int(v)

        if o=="-f":
            sep = v
        
        if o=="-F":
            fmt = v

        if o=="-h":
            usage()
            sys.exit()

    except Exception:
        usage()
        sys.stderr.write("Error parsing option: %s %s\n" % (o,v))
        raise
    
N=P+1
cfmt=" ".join([fmt]*N)

XY=[]
i=0

if verbose:
    sys.stderr.write("ix=%s iy=%s iw=%s\n" % (ix, iy, iw))

for line in fileinput.input(files):
    nn = line.split(sep)
    if not iy:
        if ix<0:
            if len(nn)<2:
                iy=[0,]
            else:
                ix=0
                iy=list(range(1,len(nn)))
        else:
            iy=[ix+1]
    try:
        if ix<0:
            x = [i]
        else:
            x = [float(eval(nn[ix]))]
        x += [float(eval(nn[i])) for i in iy]
        if iw:
            x += [float(eval(nn[i])) for i in iw]
        XY.append(x)
        i += 1
    except Exception as e:
        if verbose:
            sys.stderr.write("before item %d: %s\n" % (i,line.strip()))
            sys.stderr.write(repr(e)+"\n")

if not XY or verbose:
    sys.stderr.write("read %d items\n" % i)

XY.sort()
            
XY=numpy.array(XY)
x = XY[:,0]
y = XY[:,1:len(iy)+1].transpose()

if iw:
    w = XY[:,len(iy)+1:].transpose()
    if isσ:
        w = 1/w**2

if logscale:
    y = numpy.log(y, out=y)

p = numpy.arange(2*P+1).reshape((-1,1,1))
Ai = numpy.arange(P+1)
Ai = Ai+Ai.reshape((-1,1))

x1 = numpy.add(x, -x[0])
y1 = numpy.add(y, -y[:,0].reshape((-1,1)))
xx = numpy.power(x1, p)
xy = numpy.multiply(xx[:P+1], y1)
if iw:
    xx = numpy.multiply(w, xx)
    xy = numpy.multiply(w, xy)
sxx = numpy.add.reduce(xx, axis=-1)
sxy = numpy.add.reduce(xy, axis=-1)
d   = numpy.linalg.solve(sxx.reshape((-1,))[Ai], sxy)
d[0] += y[:,0]
if logscale:
    d[0] = numpy.exp(d[0])
    # d[1] *= d[0]

if verbose:
    sys.stderr.write("x.shape   = %s\n" % repr(x.shape))
    sys.stderr.write("y.shape   = %s\n" % repr(y.shape))
    if iw:
        sys.stderr.write("w.shape   = %s\n" % repr(w.shape))
    sys.stderr.write("xx.shape  = %s\n" % repr(xx.shape))
    sys.stderr.write("xy.shape  = %s\n" % repr(xy.shape))
    sys.stderr.write("sxx.shape = %s\n" % repr(sxx.shape))
    sys.stderr.write("sxy.shape = %s\n" % repr(sxy.shape))
    sys.stderr.write("d.shape   = %s\n" % repr(d.shape))
 
for i in range(d.shape[1]):
    sys.stdout.write(("%d "+cfmt+"\n") % ((i,)+tuple(d[:,i])))
