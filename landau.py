#!/usr/bin/python
# encoding: UTF-8

import numpy
from math import pi
from numpy import sqrt, exp

def landau(x):
    return sqrt(exp(-x-exp(-x))/2/pi)

n=100
x1=-4
x2=30

x = numpy.arange((x2-x1)*n)/float(n) + x1
l = landau(x)
N  = sum(l)/n
Ex = sum(l*x)/n/N
sx = sum(l*x*x)/n/N

print "f(x)=sqrt(exp(-x-exp(-x))/2/pi), x=[%d..%d]" % (x1,x2)
print "N=%g, <x>=%g, σ²x=%g" % (N, Ex, sx)
